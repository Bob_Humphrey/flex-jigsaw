<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="Flexbox layout examples when using the Tailwind CSS framework.">
  <title>
    Tailwind CSS Flexbox Responsive Examples
  </title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
    </head>
    <body>
        
        <div class="">
            @include('_partials.header')
        </div>

        <div class="flex justify-center w-full ">
            <div class="lg:flex w-11/12 lg:w-4/5 py-8">

                <nav class="lg:w-1/4">
                    @include('_partials.nav')
                </nav>

                <main class="lg:w-3/4">
                    <div class="">
                        <h2 class="font-sans text-blue-700 text-2xl lg:text-3xl font-bold mb-6">
                            @yield('pageTitle')
                        </h2>
                        @yield('content')
                    </div>
                </main>

            </div>
        </div>

        <footer class="">
            @include('_partials.footer')
        </footer>
    <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
    </body>
</html>