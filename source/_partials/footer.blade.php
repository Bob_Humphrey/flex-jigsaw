<div class="py-24 bg-blue-300">
    <div class="flex justify-center w-full">
        <a href="https://bob-humphrey.com/">
            <img class="w-16" src="/assets/images/bh-logo.gif" alt="Bob Humphrey website ">
        </a>
    </div>
</div>