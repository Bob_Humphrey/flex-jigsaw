<div class="flex flex-col">

    <ul class="list-reset mb-8">
        <li class="font-serif text-grey-800">
          <a class="" href="/">
            <div class="">Home</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="https://tailwindcss.com/" target="_blank" rel="noopener noreferrer">
            <div class="">Tailwind</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="https://nerdcave.com/tailwind-cheat-sheet" target="_blank" rel="noopener noreferrer">
            <div class="">Nerdcave Cheat Sheet</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="https://yoksel.github.io/flex-cheatsheet/" target="_blank" rel="noopener noreferrer">
            <div class="">Yoksel Flexbox Cheat Sheet</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="https://philipwalton.github.io/solved-by-flexbox/" target="_blank" rel="noopener noreferrer">
            <div class="">Solved by Flexbox</div>
          </a>
        </li>
    </ul>

    <h2 class="font-l text-xl font-bold text-blue-500 mb-2">Columns</h2>

    <ul class="list-reset mb-8">
        <li class="font-serif text-grey-800">
          <a class="" href="/columns/centered-container/">
            <div class="">Centered container</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/columns/centered/">
            <div class="">Centered columns</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/columns/centered-gutter/">
            <div class="">Centered columns with gutters</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/columns/left/">
            <div class="">Left justified columns</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/columns/right/">
            <div class="">Right justified columns</div>
          </a>
        </li>
    </ul>

    <h2 class="font-l text-xl font-bold text-blue-500 mb-2">Forms</h2>

    <ul class="list-reset mb-8">
        <li class="font-serif text-grey-800">
          <a class="" href="/forms/aligned-form/">
            <div class="">Aligned text input</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/forms/centered-button/">
            <div class="">Centered button</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/forms/centered-button-group/">
            <div class="">Centered button group</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/forms/previous-next/">
            <div class="">Previous/Next buttons</div>
          </a>
        </li>
    </ul>

    <h2 class="font-l text-xl font-bold text-blue-500 mb-2">Misc</h2>

    <ul class="list-reset mb-8">
      <li class="font-serif text-grey-800">
          <a class="" href="/misc/cards/">
            <div class="">Cards</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/misc/horizontal-list/">
            <div class="">Centered horizontal list</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/misc/centered-image/">
            <div class="">Centered image</div>
          </a>
        </li>
        <li class="font-serif text-grey-800">
          <a class="" href="/misc/media-object/">
            <div class="">Media object</div>
          </a>
        </li>
    </ul>

</div>
