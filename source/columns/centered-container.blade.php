@extends('_layouts.master')

@section('pageTitle')
    Centered Container
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center w-full bg-blue-300 py-10">
            <div class="bg-green-300 w-2/3 h-64">
            </div>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;div class=&#x22;bg-green-300 w-2/3 h-64&#x22;&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection