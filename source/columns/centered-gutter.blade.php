@extends('_layouts.master')

@section('pageTitle')
    Centered Columns with Gutters
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="p-10 bg-blue-300">
            <div class="lg:flex -mx-2">
                <div class="w-full lg:w-1/3 px-2 mb-4 lg:mb-0">
                    <div class="bg-green-300 h-48"></div>
                </div>
                <div class="w-full lg:w-1/3 px-2 mb-4 lg:mb-0">
                    <div class="bg-yellow-300 h-48"></div>
                </div>
                <div class="w-full lg:w-1/3 px-2">
                    <div class="bg-green-300 h-48"></div>
                </div>
            </div>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;p-10 bg-blue-300&#x22;&#x3E;
            &#x3C;div class=&#x22;lg:flex -mx-2&#x22;&#x3E;
                &#x3C;div class=&#x22;w-full lg:w-1/3 px-2 mb-4 lg:mb-0&#x22;&#x3E;
                    &#x3C;div class=&#x22;bg-green-300 h-48&#x22;&#x3E;&#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
                &#x3C;div class=&#x22;w-full lg:w-1/3 px-2 mb-4 lg:mb-0&#x22;&#x3E;
                    &#x3C;div class=&#x22;bg-yellow-300 h-48&#x22;&#x3E;&#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
                &#x3C;div class=&#x22;w-full lg:w-1/3 px-2&#x22;&#x3E;
                    &#x3C;div class=&#x22;bg-green-300 h-48&#x22;&#x3E;&#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

    

@endsection