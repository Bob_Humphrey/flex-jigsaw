@extends('_layouts.master')

@section('pageTitle')
    Right Justified Columns
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="lg:flex justify-end bg-blue-300 p-10">
            <div class="w-full lg:w-1/3 bg-yellow-300 h-48 mb-4 lg:mb-0">

            </div>
            <div class="w-full lg:w-1/3 bg-green-300 h-48">

            </div>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;h-64 flex justify-end bg-blue-300 p-10&#x22;&#x3E;
            &#x3C;div class=&#x22;w-1/3 bg-yellow-300&#x22;&#x3E;

            &#x3C;/div&#x3E;
            &#x3C;div class=&#x22;w-1/3 bg-green-300&#x22;&#x3E;

            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection