@extends('_layouts.master')

@section('pageTitle')
    Aligned Text Input
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center w-full bg-blue-300 py-10">
            <form class="bg-green-300 w-4/5 lg:w-2/3 p-10" action="">
                <div class="lg:flex mb-4 -mx-2">
                    <div class="lg:w-1/3">
                        <label class="block py-2" for="first_name">
                        First Name
                        </label>
                    </div>
                    <div class="lg:w-2/3 bg-yellow-300 py-2 px-4 rounded">
                        <input class="w-full bg-yellow-300" id="first_name" name="first_name" type="text" value="">
                    </div>
                </div>

                <div class="lg:flex mb-4 -mx-2">
                    <div class="lg:w-1/3">
                        <label class="block py-2" for="first_name">
                        Last Name
                        </label>
                    </div>
                    <div class="lg:w-2/3 bg-yellow-300 py-2 px-4 rounded">
                        <input class="w-full bg-yellow-300" id="last_name" name="last_name" type="text" value="">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;form class=&#x22;bg-green-300 w-4/5 lg:w-2/3 p-10&#x22; action=&#x22;&#x22;&#x3E;
                &#x3C;div class=&#x22;lg:flex mb-4 -mx-2&#x22;&#x3E;
                    &#x3C;div class=&#x22;lg:w-1/3&#x22;&#x3E;
                        &#x3C;label class=&#x22;block py-2&#x22; for=&#x22;first_name&#x22;&#x3E;
                        First Name
                        &#x3C;/label&#x3E;
                    &#x3C;/div&#x3E;
                    &#x3C;div class=&#x22;lg:w-2/3 bg-yellow-300 py-2 px-4 rounded&#x22;&#x3E;
                        &#x3C;input class=&#x22;w-full bg-yellow-300&#x22; id=&#x22;first_name&#x22; name=&#x22;first_name&#x22; type=&#x22;text&#x22; value=&#x22;&#x22;&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;

                &#x3C;div class=&#x22;lg:flex mb-4 -mx-2&#x22;&#x3E;
                    &#x3C;div class=&#x22;lg:w-1/3&#x22;&#x3E;
                        &#x3C;label class=&#x22;block py-2&#x22; for=&#x22;first_name&#x22;&#x3E;
                        Last Name
                        &#x3C;/label&#x3E;
                    &#x3C;/div&#x3E;
                    &#x3C;div class=&#x22;lg:w-2/3 bg-yellow-300 py-2 px-4 rounded&#x22;&#x3E;
                        &#x3C;input class=&#x22;w-full bg-yellow-300&#x22; id=&#x22;last_name&#x22; name=&#x22;last_name&#x22; type=&#x22;text&#x22; value=&#x22;&#x22;&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/form&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection