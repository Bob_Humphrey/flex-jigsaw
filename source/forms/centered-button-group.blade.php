@extends('_layouts.master')

@section('pageTitle')
    Centered Button Group
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center flex-col lg:flex-row w-full bg-blue-300 py-10">
            <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mb-2 lg:mb-0 mx-2"
                href="">
                First
            </a>
            <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mb-2 lg:mb-0 mx-2"
                href="">
                Second
            </a>
            <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mx-2"
                href="">
                Third
            </a>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center flex-col lg:flex-row w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mb-2 lg:mb-0 mx-2&#x22;
                href=&#x22;&#x22;&#x3E;
                First
            &#x3C;/a&#x3E;
            &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mb-2 lg:mb-0 mx-2&#x22;
                href=&#x22;&#x22;&#x3E;
                Second
            &#x3C;/a&#x3E;
            &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-32 mx-2&#x22;
                href=&#x22;&#x22;&#x3E;
                Third
            &#x3C;/a&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection