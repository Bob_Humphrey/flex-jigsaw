@extends('_layouts.master')

@section('pageTitle')
    Centered Button
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center w-full bg-blue-300 py-10">
            <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-48 "
                href="">
                Send
            </a>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-48 &#x22;
                href=&#x22;&#x22;&#x3E;
                Send
            &#x3C;/a&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection