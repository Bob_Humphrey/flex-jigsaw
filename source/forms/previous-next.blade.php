@extends('_layouts.master')

@section('pageTitle')
    Previous/Next Buttons
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-between bg-blue-300 p-10">
            <div class="flex justify-start"> 
                <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-24 lg:w-48"
                    href="">
                    Previous
                </a>
            </div>
            <div class="flex justify-end"> 
                <a class="bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-24 lg:w-48"
                    href="">
                    Next
                </a>
            </div>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-between bg-blue-300 p-10&#x22;&#x3E;
            &#x3C;div class=&#x22;flex justify-start&#x22;&#x3E; 
                &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-24 lg:w-48&#x22;
                    href=&#x22;&#x22;&#x3E;
                    Previous
                &#x3C;/a&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;div class=&#x22;flex justify-end&#x22;&#x3E; 
                &#x3C;a class=&#x22;bg-blue-600 hover:bg-blue-800 text-white font-serif text-center rounded py-2 w-24 lg:w-48&#x22;
                    href=&#x22;&#x22;&#x3E;
                    Next
                &#x3C;/a&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

    

@endsection














