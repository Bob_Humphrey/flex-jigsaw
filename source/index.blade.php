@extends('_layouts.master')

@section('content')

       
    <div class="lg:flex justify-center w-full -mx-4">
        <div class="hidden lg:block w-1/3 bg-blue-300 px-4">
        </div>

        <div class="w-full lg:w-1/3 px-4">
            <img class="" src="/assets/images/dog-8.jpg" alt="Dog Smile Factory ">
        </div>

        <div class="hidden lg:block w-1/3 bg-green-300 px-4">
        </div>
    </div>
    
   
@endsection
