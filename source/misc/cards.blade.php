@extends('_layouts.master')

@section('pageTitle')
    Cards
@endsection

@section('content')

    <div class="text-grey-900 text-lg mb-10">
        <div class="flex flex-wrap -mx-2">

            <div class="w-full lg:w-1/2 px-2 mb-4 flex items-stretch">
                <div class="bg-blue-300 rounded-lg">
                    <img class="rounded-t-lg" src="/assets/images/dog-8-alt.jpg" alt="Dog Smile Factory">
                    <div class="p-4">
                        <h3 class="font-bold text-blue-600 text-2xl pb-2">
                            Title
                        </h3>
                        <div class="font-serif pb-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        </div>
                        <div class="flex justify-center ">
                            <div class="text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2">
                                <a class="" href="" >
                                    Visit Site
                                </a>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full lg:w-1/2 px-2 mb-4 flex items-stretch">
                <div class="bg-blue-300 rounded-lg">
                    <img class="rounded-t-lg" src="/assets/images/dog-8-alt.jpg" alt="Dog Smile Factory">
                    <div class="p-4">
                        <h3 class="font-bold text-blue-600 text-2xl pb-2">
                            Title
                        </h3>
                        <div class="font-serif pb-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        </div>
                        <div class="flex justify-center ">
                            <div class="text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2">
                                <a class="" href="" >
                                    Visit Site
                                </a>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full lg:w-1/2 px-2 mb-4 flex items-stretch">
                <div class="bg-blue-300 rounded-lg">
                    <img class="rounded-t-lg" src="/assets/images/dog-8-alt.jpg" alt="Dog Smile Factory">   
                    <div class="p-4">
                        <h3 class="font-bold text-blue-600 text-2xl pb-2">
                            Title
                        </h3>
                        <div class="font-serif pb-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        </div>
                        <div class="flex justify-center ">
                            <div class="text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2">
                                <a class="" href="" >
                                    Visit Site
                                </a>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full lg:w-1/2 px-2 mb-4 flex items-stretch">
                <div class="bg-blue-300 rounded-lg">
                    <img class="rounded-t-lg" src="/assets/images/dog-8-alt.jpg" alt="Dog Smile Factory">
                    <div class="p-4">
                        <h3 class="font-bold text-blue-600 text-2xl pb-2">
                            Title
                        </h3>
                        <div class="font-serif pb-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        </div>
                        <div class="flex justify-center ">
                            <div class="text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2">
                                <a class="" href="" >
                                    Visit Site
                                </a>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex flex-wrap -mx-2&#x22;&#x3E;

            &#x3C;div class=&#x22;w-full lg:w-1/2 px-2 mb-4 flex items-stretch&#x22;&#x3E;
                &#x3C;div class=&#x22;bg-blue-300 rounded-lg&#x22;&#x3E;
                    &#x3C;img class=&#x22;rounded-t-lg&#x22; src=&#x22;/assets/images/dog-8-alt.jpg&#x22; alt=&#x22;Dog Smile Factory&#x22;&#x3E;
                    &#x3C;div class=&#x22;p-4&#x22;&#x3E;
                        &#x3C;h3 class=&#x22;font-bold text-blue-600 text-2xl pb-2&#x22;&#x3E;
                            Title
                        &#x3C;/h3&#x3E;
                        &#x3C;div class=&#x22;font-serif pb-4&#x22;&#x3E;
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        &#x3C;/div&#x3E;
                        &#x3C;div class=&#x22;flex justify-center &#x22;&#x3E;
                            &#x3C;div class=&#x22;text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2&#x22;&#x3E;
                                &#x3C;a class=&#x22;&#x22; href=&#x22;&#x22; &#x3E;
                                    Visit Site
                                &#x3C;/a&#x3E;
                            &#x3C;/div&#x3E;   
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;

            &#x3C;div class=&#x22;w-full lg:w-1/2 px-2 mb-4 flex items-stretch&#x22;&#x3E;
                &#x3C;div class=&#x22;bg-blue-300 rounded-lg&#x22;&#x3E;
                    &#x3C;img class=&#x22;rounded-t-lg&#x22; src=&#x22;/assets/images/dog-8-alt.jpg&#x22; alt=&#x22;Dog Smile Factory&#x22;&#x3E;
                    &#x3C;div class=&#x22;p-4&#x22;&#x3E;
                        &#x3C;h3 class=&#x22;font-bold text-blue-600 text-2xl pb-2&#x22;&#x3E;
                            Title
                        &#x3C;/h3&#x3E;
                        &#x3C;div class=&#x22;font-serif pb-4&#x22;&#x3E;
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        &#x3C;/div&#x3E;
                        &#x3C;div class=&#x22;flex justify-center &#x22;&#x3E;
                            &#x3C;div class=&#x22;text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2&#x22;&#x3E;
                                &#x3C;a class=&#x22;&#x22; href=&#x22;&#x22; &#x3E;
                                    Visit Site
                                &#x3C;/a&#x3E;
                            &#x3C;/div&#x3E;   
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;

            &#x3C;div class=&#x22;w-full lg:w-1/2 px-2 mb-4 flex items-stretch&#x22;&#x3E;
                &#x3C;div class=&#x22;bg-blue-300 rounded-lg&#x22;&#x3E;
                    &#x3C;img class=&#x22;rounded-t-lg&#x22; src=&#x22;/assets/images/dog-8-alt.jpg&#x22; alt=&#x22;Dog Smile Factory&#x22;&#x3E;   
                    &#x3C;div class=&#x22;p-4&#x22;&#x3E;
                        &#x3C;h3 class=&#x22;font-bold text-blue-600 text-2xl pb-2&#x22;&#x3E;
                            Title
                        &#x3C;/h3&#x3E;
                        &#x3C;div class=&#x22;font-serif pb-4&#x22;&#x3E;
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        &#x3C;/div&#x3E;
                        &#x3C;div class=&#x22;flex justify-center &#x22;&#x3E;
                            &#x3C;div class=&#x22;text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2&#x22;&#x3E;
                                &#x3C;a class=&#x22;&#x22; href=&#x22;&#x22; &#x3E;
                                    Visit Site
                                &#x3C;/a&#x3E;
                            &#x3C;/div&#x3E;   
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;

            &#x3C;div class=&#x22;w-full lg:w-1/2 px-2 mb-4 flex items-stretch&#x22;&#x3E;
                &#x3C;div class=&#x22;bg-blue-300 rounded-lg&#x22;&#x3E;
                    &#x3C;img class=&#x22;rounded-t-lg&#x22; src=&#x22;/assets/images/dog-8-alt.jpg&#x22; alt=&#x22;Dog Smile Factory&#x22;&#x3E;
                    &#x3C;div class=&#x22;p-4&#x22;&#x3E;
                        &#x3C;h3 class=&#x22;font-bold text-blue-600 text-2xl pb-2&#x22;&#x3E;
                            Title
                        &#x3C;/h3&#x3E;
                        &#x3C;div class=&#x22;font-serif pb-4&#x22;&#x3E;
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Curabitur eget elit eu urna gravida semper quis eget quam.
                        &#x3C;/div&#x3E;
                        &#x3C;div class=&#x22;flex justify-center &#x22;&#x3E;
                            &#x3C;div class=&#x22;text-white font-serif text-center bg-blue-600 hover:bg-blue-800 rounded px-2 py-1 mr-2 mb-2&#x22;&#x3E;
                                &#x3C;a class=&#x22;&#x22; href=&#x22;&#x22; &#x3E;
                                    Visit Site
                                &#x3C;/a&#x3E;
                            &#x3C;/div&#x3E;   
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;

        &#x3C;/div&#x3E;
    </code></pre>

    

@endsection