@extends('_layouts.master')

@section('pageTitle')
    Centered Image
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center w-full bg-blue-300 py-10">
            <a href="">
                <img class="w-16" src="/assets/images/bh-logo.gif" alt="Bob Humphrey website ">
            </a>
        </div>
    </div>

    <pre class=""><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;a href=&#x22;&#x22;&#x3E;
                &#x3C;img class=&#x22;w-16&#x22; src=&#x22;/assets/images/bh-logo.gif&#x22; alt=&#x22;Bob Humphrey website &#x22;&#x3E;
            &#x3C;/a&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

    

@endsection