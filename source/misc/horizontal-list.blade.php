@extends('_layouts.master')

@section('pageTitle')
    Centered Horizontal List
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex justify-center flex-col lg:flex-row w-full bg-blue-300 py-10">
            <a class="px-4" href="">
                Experience
            </a>
            <a class="px-4" href="">
                Recent Work
            </a>
            <a class="px-4" href="">
                Projects
            </a>
            <a class="px-4" href="">
                Skills
            </a>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex justify-center flex-col lg:flex-row w-full bg-blue-300 py-10&#x22;&#x3E;
            &#x3C;a class=&#x22;px-4&#x22; href=&#x22;&#x22;&#x3E;
                Experience
            &#x3C;/a&#x3E;
            &#x3C;a class=&#x22;px-4&#x22; href=&#x22;&#x22;&#x3E;
                Recent Work
            &#x3C;/a&#x3E;
            &#x3C;a class=&#x22;px-4&#x22; href=&#x22;&#x22;&#x3E;
                Projects
            &#x3C;/a&#x3E;
            &#x3C;a class=&#x22;px-4&#x22; href=&#x22;&#x22;&#x3E;
                Skills
            &#x3C;/a&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

    

@endsection