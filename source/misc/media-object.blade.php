@extends('_layouts.master')

@section('pageTitle')
    Media Object
@endsection

@section('content')

    <div class="font-serif text-grey-900 text-lg mb-10">
        <div class="flex flex-col lg:flex-row items-start bg-blue-300 p-10">
            <img class="w-32 mr-8 mb-4 lg:mb-0" src="/assets/images/dog-8.jpg" alt="Dog Smile Factory">
            <div class="flex-1">
                <h3 class="text-3xl font-sans font-bold text-blue-600 mb-4">Title</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra lacus 
                    quis enim feugiat pharetra. Nunc lobortis sodales nisi, et consectetur purus iaculis vel. 
                    Integer fermentum turpis vel erat euismod, et venenatis felis finibus. Cras auctor 
                    sapien tellus, sit amet tincidunt elit luctus et. Aliquam erat volutpat. Ut ut enim id nisi 
                    sodales feugiat. Sed vitae placerat magna. Interdum et malesuada fames ac ante ipsum primis 
                    in faucibus. Curabitur eu imperdiet urna. Integer gravida gravida turpis ac lobortis. 
                    Aliquam dui sapien, mollis fringilla condimentum sit amet, pellentesque ac neque. 
                </p>
            </div>
        </div>
    </div>

    <pre class="hidden md:block"><code class="language-html">
        &#x3C;div class=&#x22;flex flex-col lg:flex-row items-start bg-blue-300 p-10&#x22;&#x3E;
            &#x3C;img class=&#x22;w-32 mr-8 mb-4 lg:mb-0&#x22; src=&#x22;/assets/images/dog-8.jpg&#x22; alt=&#x22;Dog Smile Factory&#x22;&#x3E;
            &#x3C;div class=&#x22;flex-1&#x22;&#x3E;
                &#x3C;h3 class=&#x22;text-3xl font-sans font-bold text-blue-600 mb-4&#x22;&#x3E;Title&#x3C;/h3&#x3E;
                &#x3C;p&#x3E;
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra lacus 
                    quis enim feugiat pharetra. Nunc lobortis sodales nisi, et consectetur purus iaculis vel. 
                    Integer fermentum turpis vel erat euismod, et venenatis felis finibus. Cras auctor 
                    sapien tellus, sit amet tincidunt elit luctus et. Aliquam erat volutpat. Ut ut enim id nisi 
                    sodales feugiat. Sed vitae placerat magna. Interdum et malesuada fames ac ante ipsum primis 
                    in faucibus. Curabitur eu imperdiet urna. Integer gravida gravida turpis ac lobortis. 
                    Aliquam dui sapien, mollis fringilla condimentum sit amet, pellentesque ac neque. 
                &#x3C;/p&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    </code></pre>

@endsection